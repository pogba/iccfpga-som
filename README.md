# IOTA Crypto Core - Linux SoM Module

Board-files for Linux System Module of IOTA Crypto Core Project

Fourth milestone of the ICCFPGA-Projekt: https://gitlab.com/iccfpga/iccfpga-core/wikis/home

The SoM can do the following:

- [Cortex A5 SoC with 500MHz](https://www.microchip.com/wwwproducts/en/ATSAMA5D27), 128MB embedded DDR2 RAM, full Linux support
- socket for the IOTA Crypto Core FPGA Module
- 8MB QSPI-Flash (on-the-fly encryption supported)
- 10/100MBit Ethernet
- HS USB Host + Device
- SD-Card interface
- several I2Cs, SPIs, UARTs
- [ACT8865](https://active-semi.com/wp-content/uploads/ACT8865_Datasheet.pdf) PMIC (power management IC)
- size of 83x36mm
- powered by 5V

Note: JLCPCB can produce the board via the Gerber files in the repository. It's important to select impedance controlled (JLC2313).

# License
This project is licensed under the MIT-License (https://opensource.org/licenses/MIT)

# Misc

Donations are always welcomed :)

IOTA: `LLEYMHRKXWSPMGCMZFPKKTHSEMYJTNAZXSAYZGQUEXLXEEWPXUNWBFDWESOJVLHQHXOPQEYXGIRBYTLRWHMJAOSHUY`

IOTA Discord: pmaxuw#8292

